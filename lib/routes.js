if (Meteor.isClient) {
Accounts.onLogin(function() {
   FlowRouter.go('cook-book');
});

Accounts.onLogout(function(){
   FlowRouter.go('home');
});
}



FlowRouter.triggers.enter([function(context, redirect){
   if(!Meteor.userId()) {
      FlowRouter.go('home');
   }
}]);

FlowRouter.route('/', {
    name: 'home',
    action() {
      if(Meteor.userId()) {
      FlowRouter.go('cook-book');
      }
      GAnalytics.pageview();
        BlazeLayout.render('HomeLayout');
    }
});


FlowRouter.route('/cook-book', {
    name: 'cook-book',
    action() {
      GAnalytics.pageview();
        BlazeLayout.render('MainLayout', {main: 'Recipes'});
    }
});

FlowRouter.route('/cook/:id', {
    name: 'cook',
    action() {
      GAnalytics.pageview();
        BlazeLayout.render('MainLayout', {main: 'RecipeSingle'});
    }
});

FlowRouter.route('/menu', {
   name: 'menu',
   action() {
     BlazeLayout.render('MainLayout', {main: 'Menu'});
   }
});

FlowRouter.route('/shopping-list', {
   name: 'shopping-list',
   action() {
     BlazeLayout.render('MainLayout', {main: 'shoppinglist'});
   }
});
